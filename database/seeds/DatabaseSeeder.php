<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            // 'name_surname' => 'Murad Mikayilzadeh',
            // 'slug' => 'murad-mikayilzadeh-1504379801',
            // 'email' => 'm.murad@code.edu.az',
            // 'password' => bcrypt('123456'),
            // 'bonus' => 0,
            // 'status' => 1,
            // 'is_admin' => 1,
            'name' => 'Leyla Abbasova',
            'email' => 'leyla.b@code.edu.az',
            'password' => '123456'
        ]);

    }
}
