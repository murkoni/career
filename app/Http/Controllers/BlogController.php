<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use App\Tag;

class BlogController extends Controller
{
    
	public function getIndex() {
		$posts = Post::orderBy('id', 'desc')->get();
		return view('client.blog')->withPosts($posts);
	}

    public function getSingle($slug) {
    	// fetch from the DB based on slug
    	$post = Post::where('slug', '=', $slug)->first();

    	// return the view and pass in the post object
    	return view('client.post',compact('post'));
    }

    public function gallery(){
        $images = Post::select('image')->get();

        // return $images;
        return view('client.gallery',compact('images'));
    }
}
