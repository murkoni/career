<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Partner;

use Image;

class PartnerController extends Controller
{
    public function index(){
    	$partners = Partner::all();
    	return view('client.partners',compact('partners'));
    }

    public function adminIndex(){
    	$partners = Partner::all();
    	return view('partners.index',compact('partners'));
    }

    public function show($id){
        $partner = Partner::find($id);
        return view('client.partner',compact('partner'));
    }

    public function store(Request $request){

    	$new = new Partner;
    	$new->name = $request->name;
    	$new->about = $request->about;

    	if ($request->hasFile('logo')) {
          $image = $request->file('logo');
          $filename = time() . '.' . $image->getClientOriginalExtension();
          $location = public_path('images/partners/' . $filename);
          Image::make($image)->resize(800, 400)->save($location);

          $new->logo = $filename;
        }

        $new->save();
        return back();

    }

    public function delete($id){
    	$partner = Partner::find($id);
    	$partner->delete();
    	return back();
    }
}
