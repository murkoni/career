<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Sliders;

use Image;

class SliderController extends Controller
{
    public function adminIndex(){

        $slides = Sliders::all();
        return view('slider.index',compact('slides'));
    }

    public function store(Request $request){

        $this->validate($request,[
            'heading'=>'required',
            'description'=>'required',
        ]);

        $new = new Sliders;
        $new->heading = $request->heading;
        $new->description = $request->description;
        
        if ($request->hasFile('image')) {
          $image = $request->file('image');
          $filename = time() . '.' . $image->getClientOriginalExtension();
          $location = public_path('images/slider/' . $filename);
          Image::make($image)->save($location);

          $new->image = $filename;
        }

        $new->save();
        return back()->with('success','Slide added succesfully!');

    }

    // public function editSlide($id){

    //     $slide = Slider::findOrFail($id);
    //     return view('admin.edit.edit-slide',compact('slide'));

    // }

    // public function editSaveSlide(Request $request,$id){

    //     $this->validate($request,[
    //         'title'=>'required',
    //         'description'=>'required',
    //     ]);
    //     $slide = Slider::findOrFail($id);
    //     $slide->heading = $request->title;
    //     $slide->description = $request->description;
    //     if($request->hasFile('image')){
    //         $ext=$request->file('image')->getClientOriginalExtension();
    //         if($ext=='jpg' || $ext=='png' || $ext=='jpeg' || $ext=='bmp')  {
    //             $file=$request->file('image');
    //             $filename=time().'.'.$file->getClientOriginalExtension();
    //             $file->move('assets/images/media/slider/',$filename);
    //             $path='assets/images/media/slider/'.$filename;

    //             $slide->image = $path;
    //         }
    //     }
    //     $slide->save();
    //     return back()->with('success','Slide redaktə edildi');

    // }

    public function delete($id){

        $slide = Sliders::findOrFail($id);
        $slide->delete();
        return back();

    }
}
