<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Events;
use Image;


class EventController extends Controller
{
    public function index(){
    	$events = Events::all();
    	return view('client.events',compact('events'));
    }

    public function adminIndex(){
    	$events = Events::all();
    	return view('events.index',compact('events'));
    }

    public function show($id){
        $event = Events::find($id);
        return view('client.event',compact('event'));
    }

    public function store(Request $request){

    	$new = new Events;
    	$new->title = $request->name;
    	$new->about = $request->about;

    	if ($request->hasFile('logo')) {
          $image = $request->file('logo');
          $filename = time() . '.' . $image->getClientOriginalExtension();
          $location = public_path('images/events/' . $filename);
          Image::make($image)->resize(800, 400)->save($location);

          $new->photo = $filename;
        }

        $new->time = $request->time;

        $new->save();
        return back();

    }

    public function delete($id){
    	$event = Events::find($id);
    	$event->delete();
    	return back();
    }
}
