<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
	// Authentication Routes
	Route::get('mezunadmin',function(){
		return view('special.index');
	});

	Route::get('mezunadmin/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
	Route::post('mezunadmin/login', 'Auth\AuthController@postLogin');
	Route::get('mezunadmin/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);


	// Categories
	Route::resource('categories', 'CategoryController', ['except' => ['create']]);
	Route::resource('tags', 'TagController', ['except' => ['create']]);

	
    Route::get('contact', 'PagesController@getContact');
    Route::post('contact', 'PagesController@postContact');
	Route::get('about', 'PagesController@getAbout');
	Route::get('/', 'PagesController@getIndex');
	Route::resource('posts', 'PostController');

	//Partners
	Route::get('/mezunadmin/partners','PartnerController@adminIndex');
	Route::post('/mezunadmin/partners/add','PartnerController@store');
	Route::get('/mezunadmin/partners/delete/{id}','PartnerController@delete');

	//Events
	Route::get('/mezunadmin/events','EventController@adminIndex');
	Route::post('/mezunadmin/events/add','EventController@store');
	Route::get('/mezunadmin/events/delete/{id}','EventController@delete');

	//Slider
	Route::get('/mezunadmin/slider','SliderController@adminIndex');
	Route::post('/mezunadmin/slider/add','SliderController@store');
	Route::get('/mezunadmin/slider/delete/{id}','SliderController@delete');


});


Route::get('/', 'PagesController@getindex');

// Route::get('/blog', function () {
//     return view('client.blog');
// });

Route::get('blog/{slug}', ['as' => 'blog.single', 'uses' => 'BlogController@getSingle'])->where('slug', '[\w\d\-\_]+');
Route::get('blog', ['uses' => 'BlogController@getIndex', 'as' => 'blog.index']);

Route::get('/contact', function () {
    return view('client.contact');
});

Route::get('/gallery', 'BlogController@gallery');

Route::get('/partners', 'PartnerController@index');
Route::get('/partner/show/{id}','PartnerController@show');

Route::get('/events', 'EventController@index');
Route::get('/events/show/{id}','EventController@show');

// Auth::routes();

