<!DOCTYPE html>
<html lang="en">
<head>

  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic">
  <title>ADNSU/CareerCenter</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <!--meta info-->
  <meta name="author" content="">
  <meta name="keywords" content="">
  <meta name="description" content="">
  <!-- <link rel="shortcut icon" type="image/x-icon" href="assets/images/fav_icon.ico"> -->
  <!--stylesheet include-->
  <link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
  <link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/css/responsive.css')}}">
  <link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/css/styleswitcher.css')}}">
  <link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/css/colorpicker.css')}}">
  <link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/css/owl.carousel.css')}}">
  <link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/css/animate.css')}}">
  <link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/plugins/layerslider/css/layerslider.css')}}">
  <!--modernizr-->
  <script src="{{asset('assets/js/jquery.modernizr.js')}}"></script>
  
</head>
<body class="wide_layout">
  <div class="wrapper_container">
    <!--==============================header=================================-->
    <!-- login esas -->
    <header role="banner" class="header header-main">
      <div class="h_top_part">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="header_top mobile_menu var2">
                <nav>
                  <ul>
                    <li><a href="index.php">Ana səhifə</a></li>
                    <li><a href="kontakt.php">Əlaqə</a></li>
                  </ul>
                </nav>
                <div class="login_block">
                  <ul>
                    <!--Login-->
                <!--     <li class="login_button">
                      <a href="#" role="button"><i class="fa fa-user login_icon"></i>Giriş et</a>
                      <div class="popup">
                        <form>
                          <ul>
                            <li>
                              <label for="username">İstifadəçi adı</label><br>
                              <input type="text" name="" id="username">
                            </li>
                            <li>
                              <label for="password">Şifrə</label><br>
                              <input type="password" name="" id="password">
                            </li>
                            <li>
                              <input type="checkbox" id="checkbox_10"><label for="checkbox_10">yadda saxla</label>
                            </li>
                            <li>
                              <a href="#" class="button button_orange">Giriş et</a>
                              <div class="t_align_c">
                                <a href="#" class="color_dark">Şifrəni unutmusunuz?</a><br>
                                <a href="#" class="color_dark">İstifadəçi adını unutmusunuz?</a>
                              </div>
                            </li>
                          </ul>
                        </form>
                        <section class="login_footer">
                          <h3>Yeni istifadəçi?</h3>
                          <a href="#" class="button button_grey">Hesab yarat</a>
                        </section>
                      </div>       
                    </li> -->
                    <!--language settings-->
                    <li class="lang_button">
                      <a role="button" href="index.php"><img src="assets/images/flag_g.jpg" alt=""><span>Az</span></a>
                      <ul class="dropdown_list">
                       <li><a href="index.php"><img src="assets/images/flag_g.jpg" alt="">Az</a></li> 
                       <li><a href="en/index.php"><img src="assets/images/flag_en.jpg" alt="">İng</a></li>
                       <li><a href="ru/index.php"><img src="assets/images/flag_s.jpg" alt="">Ru</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<!--login esas   son -->

      <!-- esas -->
      <div class="h_bot_part">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="clearfix">
                <a href="index.php" class="f_left logo"><h1 style="color: #D1008B;">KARYERA <br> MƏRKƏZİ</h1><!-- <img src="assets/images/logo_main.png" alt=""> --></a>
                <a href="#" class="f_right"><img src="assets/images/728x90.jpg" alt=""></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- esas son -->







      <!--main menu container-->
      <div class="menu_wrap">
        <div class="menu_border">
          <div class="container clearfix menu_border_wrap">
            <!--button for responsive menu-->
            <button id="menu_button">
              Menu
            </button>
            <!--main menu-->
            <nav role="navigation" class="main_menu menu_var2">  
              <ul>
            <!--  class="current" -->     
                    
                    
               
                <li class="menu_1"><a href="#">Məzun və tələbələr üçün<span class="plus"><i class="fa fa-plus-square-o"></i><i class="fa fa-minus-square-o"></i></span></a>
                   
                </li>
                <li class="menu_2"><a href="#">İşəgötürənlər üçün<span class="plus"><i class="fa fa-plus-square-o"></i><i class="fa fa-minus-square-o"></i></span></a>
                   
                </li>
                <li class="menu_3"><a href="{{url('/events')}}">Tədbİrlər<span class="plus"><i class="fa fa-plus-square-o"></i><i class="fa fa-minus-square-o"></i></span></a>
                   
                </li>
                <li class="menu_4"><a href="{{url('/partners')}}">Tərəfdaşlar<span class="plus"><i class="fa fa-plus-square-o"></i><i class="fa fa-minus-square-o"></i></span></a>
                
                  
                </li>
                <li class="menu_5"><a href="{{url('/gallery')}}">Qalereya<span class="plus"><i class="fa fa-plus-square-o"></i><i class="fa fa-minus-square-o"></i></span></a>
                  <!--sub menu-->
                 
                </li>
                
                
                </li>
              
                  
                </li>
                <li class="menu_8"><a href="#">CV bankı<span class="plus"><i class="fa fa-plus-square-o"></i><i class="fa fa-minus-square-o"></i></span></a>
              
                </li>
<li class="menu_8"><a href="{{url('/contact')}}">Əlaqə<span class="plus"><i class="fa fa-plus-square-o"></i><i class="fa fa-minus-square-o"></i></span></a>
              </ul>
            </nav>
            <div class="search-holder">
              <div class="search_box">
                <div class="shopping_button button_orange_hover">
                  
                  <div class="shopping_cart">
                    <div class="sc_header">Recently added item(s)</div>
                    <div class="sc_footer">
                      
                    </div>
                  </div>
                </div>
                <button class="search_button button button_orange_hover">
                  <i class="fa fa-search"></i>
                </button>
              </div>
              <!--search form-->
              <div class="searchform_wrap var2">
                <div class="container vc_child h_inherit relative">
                  <form role="search">
                    <input type="text" name="search" placeholder="Axtarış">
                  </form>
                  <button class="close_search_form">
                    <i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    @yield('content')

        <!--==============================footer=================================-->
    <!--markup footer-->
    <footer class="footer footer-main">
      <div class="footer_top_part">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="widget widget_text appear-animation fadeInDown appear-animation-visible" data-appear-animation="fadeInDown" data-appear-animation-delay="1150" style="-webkit-animation: 1150ms;">
                <a href="index.php" class="logo"><h1 style="color: #D1008B;">KARYERA <br> MƏRKƏZİ</h1><!-- <img src="assets/images/logo_main.png" alt=""> --></a>
                <p>Azərbaycan Dövlət Neft və Sənaye Universitetində (ADNSU) Məzun və Karyera Mərkəzi 2016-cı ildə yaradılıb. Bu günədək 2 dəfə Məzun-Karyera Sərgisi keçirilib və 100-dən artıq tələbə və məzun işlə təmin olunub.</p> <br>
               
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="widget widget_categories appear-animation fadeInDown appear-animation-visible" data-appear-animation="fadeInDown" data-appear-animation-delay="1150" style="-webkit-animation: 1150ms;">
                <h3 class="widget_title">Ünvan</h3>
                Azərbaycan Dövlət Neft və Sənaye Universiteti Azadlıq prospekti 20, AZ1010 Bakı
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="widget widget_categories appear-animation fadeInDown appear-animation-visible" data-appear-animation="fadeInDown" data-appear-animation-delay="1150" style="-webkit-animation: 1150ms;">
                <h3 class="widget_title">Əlaqə</h3>
                Tel: +99412 493 14 63 <br>
                Fax: +99412 598 29 41
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="widget widget_newsletter form_section appear-animation fadeInDown appear-animation-visible" data-appear-animation="fadeInDown" data-appear-animation-delay="1150" style="-webkit-animation: 1150ms;">
                <h3 class="widget_title">E-Poçt</h3>
                <div class="form_text">info@asoiu.edu.az</div>
                
              </div>
              <div class="widget widget_social_icons clearfix appear-animation fadeInDown appear-animation-visible" data-appear-animation="fadeInDown" data-appear-animation-delay="1150" style="-webkit-animation: 1150ms;">
                <h3 class="widget_title">Bizi izləyin</h3>
                <ul>
                  <li class="facebook">
                    <span class="tooltip">Facebook</span>
                    <a href="#">
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li class="twitter">
                    <span class="tooltip">Twitter</span>
                    <a href="#">
                      <i class="fa fa-twitter"></i>
                    </a>
                  </li>
                 <!--  <li class="google_plus">
                    <span class="tooltip">Google+</span>
                    <a href="#">
                      <i class="fa fa-google-plus"></i>
                    </a>
                  </li> -->
                  <!-- <li class="rss">
                    <span class="tooltip">Rss</span>
                    <a href="#">
                      <i class="fa fa-rss"></i>
                    </a>
                  </li> -->
                  <!-- <li class="pinterest">
                    <span class="tooltip">Pinterest</span>
                    <a href="#">
                      <i class="fa fa-pinterest"></i>
                    </a>
                  </li> -->
                  <li class="instagram">
                    <span class="tooltip">Instagram</span>
                    <a href="#">
                      <i class="fa fa-instagram"></i>
                    </a>
                  </li>
                  <!-- <li class="linkedin">
                    <span class="tooltip">LinkedIn</span>
                    <a href="#">
                      <i class="fa fa-linkedin"></i>
                    </a>
                  </li> -->
                  <!-- <li class="vimeo">
                    <span class="tooltip">Vimeo</span>
                    <a href="#">
                      <i class="fa fa-vimeo-square"></i>
                    </a>
                  </li> -->
                  <li class="youtube">
                    <span class="tooltip">Youtube</span>
                    <a href="#">
                      <i class="fa fa-youtube-play"></i>
                    </a>
                  </li>
                  <!-- <li class="flickr">
                    <span class="tooltip">Flickr</span>
                    <a href="#">
                      <i class="fa fa-flickr"></i>
                    </a>
                  </li> -->
                  <li class="envelope">
                    <span class="tooltip">E-poçt</span>
                    <a href="#">
                      <i class="fa fa-envelope-o"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
     
      <!--copyright part-->
      <div class="footer_bottom_part">
        <div class="container clearfix">
          <p>&copy; 2017 <span>ADNSU CAREER CENTER</span>. Bütün hüquqlar qorunur.</p>
          <div class="mobile_menu var2">
            <nav>
              <ul>
              
                <li><a href="#">Xəbərlər</a></li>
                <li><a href="#">Məzun və tələbələr üçün</a></li>
                <li><a href="#">İşəgötürənlər üçün</a></li>
                <li><a href="#">Tədbirlər</a></li>
                <li><a href="#">Tərəfdaşlar</a></li>
                <li><a href="#">Qalereya</a></li>
                <li><a href="#">CV bank</a></li>
                <li><a href="#">Əlaqə</a></li>
               
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </footer>
  </div>

  <!--scripts include-->
  <script src="{{url('assets/js/jquery-2.1.0.min.js')}}"></script>
  <script src="{{url('assets/js/jquery-ui.min.js')}}"></script>
  <script src="{{url('assets/js/jquery.queryloader2.min.js')}}"></script>
  <script src="{{url('assets/js/jflickrfeed.js')}}"></script>
  <script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
  <script src="{{url('assets/js/retina.js')}}"></script>
  <script src="{{url('assets/js/apear.js')}}"></script>
  <script src="{{url('assets/js/styleswither.js')}}"></script>
  <script src="{{url('assets/js/colorpicker.js')}}"></script>
  <script src="{{url('assets/js/circles.min.js')}}"></script>
  <script src="{{url('assets/plugins/twitter/jquery.tweet.min.js')}}"></script>
  <script src="{{url('assets/plugins/layerslider/js/greensock.js')}}"></script>
  <script src="{{url('assets/plugins/layerslider/js/layerslider.kreaturamedia.jquery.js')}}"></script>
  <script src="{{url('assets/plugins/layerslider/js/layerslider.transitions.js')}}"></script>
  <script src="{{url('assets/js/plugins.js')}}"></script>
  <script src="{{url('assets/js/script.js')}}"></script>

</body>
</html>