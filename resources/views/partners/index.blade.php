@extends('main')

@section('title', '| All Partners')

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>All Partners</h1>
		</div>

		<div class="col-md-2">
			{{-- <a href="{{ route('posts.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Create New Post</a> --}}
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">
			<form action="{{url('/mezunadmin/partners/add')}}" method="post" enctype="multipart/form-data">
				<h2>Add Partner</h2>
				
				{{csrf_field()}}

				<input type="text" name="name" class="form-control" placeholder="Name">
				<br>
				<textarea placeholder="About..." name="about" class="form-control" cols="30" rows="10"></textarea>
				<br>
				<input type="file" name="logo" class="form-control">
				<br>
				<input type="submit" value="Add" class="btn btn-success">
				<br><br><br>

			</form>
		</div>

		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Name</th>
					<th>About</th>
					<th>Logo</th>
					<th>Created at</th>
					<th>Actions</th>
				</thead>

				<tbody>
					
					@foreach ($partners as $partner)
						
						<tr>
							<th>{{ $partner->id }}</th>
							<td>{{ $partner->name }}</td>
							<td>{{ substr($partner->about,0,100)}}</td>
							<td>
								<img src="/images/partners/{{$partner->logo}}" style="max-width: 50px; max-height: 50px;">
							</td>
							<td>{{ date('M j, Y', strtotime($partner->created_at)) }}</td>
							<td><a href="{{url('mezunadmin/partners/delete',$partner->id)}}" class="btn btn-danger btn-sm">Delete</a></td>
							
						</tr>

					@endforeach

				</tbody>
			</table>

			<div class="text-center">
				{{-- {!! $posts->links(); !!} --}}
			</div>
		</div>
	</div>

@stop