@extends('layouts.header_footer')

@section('content')
   
    <!--==============================content================================-->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-sm-12">

            <div class="section read_post_list gallery_list">
              <h2 class="section_title section_title_big">Tərəfdaşlarımız</h2>


              <ul class="row">
                <!--Post-->
                @foreach($partners as $partner)
                <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                  <div class="section_post_left">
                    <div class="scale_image_container with_buttons">
                      <a href="#"><img src="/images/partners/{{$partner->logo}}" alt="" class="scale_image"></a>
                      <div class="open_buttons clearfix">
                        <div class="f_left"><a href="{{url('/partner/show',$partner->id)}}" role="button" class="jackbox jackbox_button button button_grey_light" data-group="gallery_1"><i class="fa fa-search-plus"></i></a></div>
                      </div>
                    </div>
                  </div>
                </li>
                @endforeach

              </ul>

            </div>

          </div>
          <div class="col-lg-4 col-md-4 col-sm-12">
            <!--Featured video-->
            <div class="section">
              <h3 class="section_title">Video</h3>
              <div class="t_align_c">
                <div class="iframe_video_container">
                  <iframe src="http://player.vimeo.com/video/64473966?title=0&amp;byline=0&amp;portrait=0&amp;color=dedede" allowfullscreen></iframe>
                </div>
              </div>
            </div>
            <div class="section t_align_c">
              <a href="#" class="m_top_50 d_block"><img src="images/336x280.jpg" alt=""></a>
            </div>
            <div class="section">
              <h3 class="section_title">Sosial Mediya</h3>
              <ul class="social_media_list clearfix">
                <li>
                  <a href="#" class="rss">
                    <i class="fa fa-rss"></i>
                    <div>135</div>
                    <p>İzləyici</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="fb">
                    <i class="fa fa-facebook"></i>
                    <div>794</div>
                    <p>İzləyici</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="g_plus">
                    <i class="fa fa-google-plus"></i>
                    <div>941</div>
                    <p>İzləyici</p>
                  </a>
                </li>
               
               
                
              </ul>
            </div>
         <!--    <div class="section">
              <h3 class="section_title">Meat Our Writers</h3>
              <ul class="writers_list clearfix">
                <li>
                  <a href="#">
                    <div>
                      <img src="assets/images/writer_1.jpg" alt="">
                    </div>
                    <div class="post_text">
                      <h4>Jessica Priston</h4>
                      <div class="event_date">Writer</div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div>
                      <img src="assets/images/writer_2.jpg" alt="">
                    </div>
                    <div class="post_text">
                      <h4>John Franklin</h4>
                      <div class="event_date">Contributing Editor</div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div>
                      <img src="assets/images/writer_3.jpg" alt="">
                    </div>
                    <div class="post_text">
                      <h4>Camala Haddon</h4>
                      <div class="event_date">Writer</div>
                    </div>
                  </a>
                </li>
              </ul>
            </div> -->
          <!--   <div class="section form_section">
              <h3 class="section_title">Newsletter Sign Up</h3>
              <div class="form_text">Sign up to our newsletter and get exclusive deals you will not find anywhere else straight to your inbox!</div>
              <form id="newsletter">
                <button type="submit" class="btn-email button button_grey" data-type="submit"><i class="fa fa-envelope-o"></i></button>
                <div class="wrapper">
                  <input type="email" placeholder="Your email address" name="newsletter-email">
                </div> 
              </form>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <!--==============================footer=================================-->
    <!--markup footer-->
@endsection