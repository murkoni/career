@extends('layouts.header_footer')

@section('content')
    <!--==============================content================================-->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-sm-12">
            <div class="section">
              <h2 class="section_title section_title_big">Bizimlə əlaqə</h2>
              <p>Burada informasiya olacaq.Burada informasiya olacaq.Burada informasiya olacaq.Burada informasiya olacaq.Burada informasiya olacaq.Burada informasiya olacaq. <span class="required">*</span>.</p>
              <form id="contactform" class="contact_form">
                <ul>
                  <li>
                    <label>Ad<span class="required">*</span></label>
                    <input type="text" name="cf_name" id="cf_name">
                  </li>
                  <li>
                    <label>E-poçt<span class="required">*</span></label>
                    <input type="text" name="cf_email" id="cf_email">
                  </li>
                  <li>
                    <label>Mövzu</label>
                    <input type="url" name="cf_subject" id="cf_subject">
                  </li>
                  <li>
                    <label>Mətn</label>
                    <textarea name="cf_message" id="cf_message"></textarea>
                  </li>
                  <li>
                    <button class="button button_grey">Göndər</button>
                  </li>
                </ul>
              </form>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12">
            <!--Featured video-->
            <div class="section">
              <h3 class="section_title">Əlaqə detalları</h3>
              <div class="t_align_c">
                <div class="map_container">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12098.04228269596!2d-74.00499255597757!3d40.70677554722762!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2z0J3RjNGOLdCZ0L7RgNC6!5e0!3m2!1sru!2s!4v1393474990482"></iframe>
                </div>
              </div>
              
              <ul class="contact_info_list">
                <li>
                  <div class="clearfix">
                    <i class="fa fa-map-marker"></i>
                    <p>Azərbaycan Dövlət Neft və Sənaye Universiteti Azadlıq küç 20, AZ1010 Bakı</p>
                  </div>
                </li>
                <li>
                  <div class="clearfix">
                    <i class="fa fa-phone"></i>
                    <p>Tel: +99412 493 14 63 <br>
                Fax: +99412 598 29 41 </p>
                  </div>
                </li>
                <li>
                  <div class="clearfix m_bottom_10">
                    <i class="fa fa-envelope"></i>
                    <p>
                      <a href="mailto:#">info@asoiu.edu.az</a>
                    </p>
                  </div>
                </li>
              </ul>
            </div>
            <div class="section">
              <h3 class="section_title">Sosial Mediya</h3>
              <ul class="social_media_list clearfix">
                <li>
                  <a href="#" class="rss">
                    <i class="fa fa-rss"></i>
                    <div>135</div>
                    <p>İzləyici</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="fb">
                    <i class="fa fa-facebook"></i>
                    <div>794</div>
                    <p>İzləyici</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="g_plus">
                    <i class="fa fa-google-plus"></i>
                    <div>941</div>
                    <p>İzləyici</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="you_tube">
                    <i class="fa fa-youtube-play"></i>
                    <div>820</div>
                    <p>İzləyici</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="twitter">
                    <i class="fa fa-twitter"></i>
                    <div>562</div>
                    <p>İzləyici</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="pint">
                    <i class="fa fa-pinterest"></i>
                    <div>1,310</div>
                    <p>İzləyici</p>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection