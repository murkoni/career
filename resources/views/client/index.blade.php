@extends('layouts.header_footer')

@section('content')
    <!--==============================content================================-->
    <div class="content">      
      <div class="container">
        <div class="row">

          <!--==============================main content=================================-->
          <main id="main" class="col-md-8 col-sm-12">
            <div class="section">
        <!-- ========layerslider======== -->
        <div id="layerslider" class="section_8" style="width: 100%; height: 500px;" data-appear-animation="fadeInDown" data-appear-animation-delay="1150">
          
          @foreach($slides as $slide)
          <div class="ls-layer slide1" style="slidedirection: top; slidedelay: 8000; durationin: 1500; durationout: 1500; easingin: easeInOutQuint; easingout: easeInOutQuint; delayin: 0; delayout: 0; transition3d: all;"> <img src="/images/slider/{{$slide->image}}" class="ls-bg" alt="Slide background">
            <div class="ls-s4  slide-text1" style="position: absolute; top: 50%; right: 0; slidedirection : bottom; slideoutdirection : top;  durationin : 1500; durationout : 1500; easingin: easeInOutQuint; easingout : easeInOutQuint; delayin : 0; delayout : 100; showuntil : 0;">
              <div class="caption_inner layer_slide_text">
                <div class="clearfix">
                 <!--  <a href="#" role="button" class="button banner_button sport">Sport</a> -->
                  <!-- <div class="event_date">Noyabr 12, 2017 5:50 am</div> -->
                </div>
                <a href="#"><h2>{{$slide->heading}}</h2></a>
                <p>{!!$slide->description!!}</p>
              </div>
            </div>
          </div>
          @endforeach

        </div>

        <!--========end layerslider========--> 







              <div class="tabs variation_2 variation_4" data-appear-animation="fadeInDown" data-appear-animation-delay="1150">
                <!--tabs navigation-->
                <div class="clearfix">
                  <h3 class="section_title">Tədbİrlər</h3>
                  <div class="clearfix tabs_conrainer">
                    <ul class="tabs_nav clearfix">
                      
                      <!-- <li class=""><a href="#tab-4"></a></li> -->
                    </ul>
                  </div>
                </div>
                <!--tabs content-->
                <div class="tabs_content post_var_inline">
                  <div id="tab-1">
                    <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <ul>

                          @if(isset($events[0]))
                          <li class="clearfix">
                            <div class="scale_image_container">
                              <a href="{{url('/events/show',$events[0]->id)}}"><img src="/images/events/{{$events[0]->photo}}" alt="" class="scale_image"></a>
                              <div class="post_image_buttons">
                                <a href="#" class="icon_box">
                                  <i class="fa fa-picture-o"></i>
                                </a>
                              </div>
                            </div>
                            <div class="post_text">
                              <a href="{{url('/events/show',$events[0]->id)}}"><h4>{{$events[0]->title}}</h4></a>
                              <div class="event_date">{{$events[0]->time}}</div>
                            </div>
                          </li>
                          @endif

                          @if(isset($events[1]))
                          <li class="clearfix">
                            <div class="scale_image_container">
                              <a href="{{url('/events/show',$events[1]->id)}}"><img src="/images/events/{{$events[1]->photo}}" alt="" class="scale_image"></a>
                              <div class="post_image_buttons">
                                <a href="#" class="icon_box">
                                  <i class="fa fa-picture-o"></i>
                                </a>
                              </div>
                            </div>
                            <div class="post_text">
                              <a href="{{url('/events/show',$events[1]->id)}}"><h4>{{$events[1]->title}}</h4></a>
                              <div class="event_date">{{$events[1]->time}}</div>
                            </div>
                          </li>
                          @endif

                        </ul>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <ul>

                          @if(isset($events[2]))
                          <li class="clearfix">
                            <div class="scale_image_container">
                              <a href="{{url('/events/show',$events[2]->id)}}"><img src="/images/events/{{$events[2]->photo}}" alt="" class="scale_image"></a>
                              <div class="post_image_buttons">
                                <a href="#" class="icon_box">
                                  <i class="fa fa-picture-o"></i>
                                </a>
                              </div>
                            </div>
                            <div class="post_text">
                              <a href="{{url('/events/show',$events[2]->id)}}"><h4>{{$events[2]->title}}</h4></a>
                              <div class="event_date">{{$events[2]->time}}</div>
                            </div>
                          </li>
                          @endif

                          @if(isset($events[3]))
                          <li class="clearfix">
                            <div class="scale_image_container">
                              <a href="{{url('/events/show',$events[3]->id)}}"><img src="/images/events/{{$events[3]->photo}}" alt="" class="scale_image"></a>
                              <div class="post_image_buttons">
                                <a href="#" class="icon_box">
                                  <i class="fa fa-picture-o"></i>
                                </a>
                              </div>
                            </div>
                            <div class="post_text">
                              <a href="{{url('/events/show',$events[3]->id)}}"><h4>{{$events[3]->title}}</h4></a>
                              <div class="event_date">{{$events[3]->time}}</div>
                            </div>
                          </li>
                          @endif

                        </ul>
                      </div>
                    </div>
                  </div>                 
                </div>
              </div>
            </div>
            
            <!-- placeholder -->
            <!-- <div class="section t_align_c" data-appear-animation="fadeInDown" data-appear-animation-delay="1150">
              <a href="#"><img src="assets/images/728x90.jpg" alt=""></a>
            </div> -->
            <div class="section_8">
        <div class="news_gallery news_gallery_var2" data-appear-animation="fadeInDown" data-appear-animation-delay="1150" style="background:white;">
          <div class="container" style="background:#f2f2f2; padding:20px;">
            <h3 class="section_title">Xəbərlər</h3>
            <!--Gallery-->
            <div id="owl-demo-5">
              
              @foreach($posts2 as $post2)
              <div class="item">
                <div class="scale_image_container">
                  <a href="{{url('/blog',$post2->slug)}}"><img src="../images/posts/{{$post2->image}}" alt="" class="scale_image"></a>
                  <!--caption-->
                  <div class="caption_type_1">
                    <div class="caption_inner">
                      <a href="{{url('/blog',$post2->slug)}}"><h4>{{$post2->title}}</h4></a>
                      <div class="event_date">{{date('M j, Y', strtotime($post2->created_at))}}</div>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach

            </div>
          </div>
        </div>
      </div>
            <!-- tabs travel -->
          

            <!-- placeholder -->
            <div class="section t_align_c" data-appear-animation="fadeInDown" data-appear-animation-delay="1150">
          <!--     <a href="#"><img src="assets/images/728x90.jpg" alt=""></a> -->
            </div>

            <!-- tabs fashion -->
         
          </main>

          <!--==============================sidebar=================================-->
          <aside id="sidebar" class="col-md-4 col-sm-12">
            <!--placeholder-->
        

            <!--tabs-->
            <div class="section" data-appear-animation="fadeInDown" data-appear-animation-delay="1150">
              <div class="tabs">
                <!--tabs navigation-->
                <div class="clearfix tabs_conrainer">
                  <ul class="tabs_nav clearfix">
                    <li class=""><h3>Sonuncu xəbərlər</h3></li>            
                  </ul>
                </div>
                <!--tabs content-->
                <div class="tabs_content post_var_inline side_bar_tabs">
                  <div id="tab-18">
                    <ul>
                    @foreach($posts as $post)
                      <li class="clearfix">
                        <div class="scale_image_container">
                          <a href="#"><img src="../images/posts/{{$post->image}}" alt="" class="scale_image"></a>
                          <div class="post_image_buttons">
                            {{-- <a href="#" class="button banner_button fashion">Təhsil</a> --}}
                            {{-- <a href="#" class="icon_box"> --}}
                              <!-- <i class="fa fa-video-camera"></i> -->
                            {{-- </a> --}}
                          </div>
                          {{-- <div class="canvas canvas_small">
                            <div class="circle" id="circles-4"></div>
                            <br />
                          </div> --}}
                        </div>
                        <div class="post_text">
                          <a href="{{url('/blog',$post->slug)}}"><h4>{{$post->title}}</h4></a>
                          <div class="event_date">{{date('M j, Y', strtotime($post->created_at))}}</div>
                        </div>
                      </li>
                    @endforeach  
                    </ul>
                  </div>                  
                </div>
              </div>
            </div>
            

            

            
            <!--placeholders-->
          

            <!--social media-->
            <div class="section" data-appear-animation="fadeInDown" data-appear-animation-delay="1150">
              <h3 class="section_title">Sosial medya</h3>
              <ul class="social_media_list clearfix">
                <li>
                  <a href="#" class="rss">
                    <i class="fa fa-rss"></i>
                    <div>1000</div>
                    <p>İzləyİcİ</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="fb">
                    <i class="fa fa-facebook"></i>
                    <div>2000</div>
                    <p>İzləyİcİ</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="g_plus">
                    <i class="fa fa-google-plus"></i>
                    <div>941</div>
                    <p>İzləyİcİ</p>
                  </a>
                </li>



              </ul>
            </div>


    <div class="section t_align_c" " data-appear-animation="fadeInDown" data-appear-animation-delay="1150">

            </div>

          </aside>
        </div>

        <!-- placeholder -->
        <div class="section t_align_c" data-appear-animation="fadeInDown" data-appear-animation-delay="1150">
          <a href="#"><img src="assets/images/970x90_light.jpg" alt=""></a>
        </div>
      </div>
    </div>
    
    <!--scripts include-->
  <script src="assets/js/jquery-2.1.0.min.js"></script>
  <script src="assets/js/jquery-ui.min.js"></script>
  <script src="assets/js/jquery.queryloader2.min.js"></script>
  <script src="assets/js/jflickrfeed.js"></script>
  <script src="assets/js/retina.js"></script>
  <script src="assets/js/apear.js"></script>
  <script src="assets/js/styleswither.js"></script>
  <script src="assets/js/colorpicker.js"></script>
  <script src="assets/js/circles.min.js"></script>
  <script src="assets/plugins/twitter/jquery.tweet.min.js"></script>
  <script src="assets/plugins/layerslider/js/greensock.js"></script>
  <script src="assets/plugins/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
  <script src="assets/plugins/layerslider/js/layerslider.transitions.js"></script>
  <script src="assets/js/plugins.js"></script>
  <script src="assets/js/script.js"></script>

      <script>
    var colors = [['#fa985d', '#ffffff']], circles = [];
    var child = document.getElementById('circles-3');
    circles.push(Circles.create({
        id:         child.id,
        value:      6.2,
        radius:     14,
        width:      3,
        maxValue:   10,
        duration:   1000,
        text:       function(value){return value;},
        colors:    ['#fa985d', '#ffffff']
    }));

    var child = document.getElementById('circles-4');
    circles.push(Circles.create({
        id:         child.id,
        value:      6.2,
        radius:     14,
        width:      3,
        maxValue:   10,
        duration:   1000,
        text:       function(value){return value;},
        colors:    ['#fa985d', '#ffffff']
    }));

    var child = document.getElementById('circles-8');
    circles.push(Circles.create({
        id:         child.id,
        value:      8.0,
        radius:     19,
        width:      3,
        maxValue:   10,
        duration:   1000,
        text:       function(value){return value;},
        colors:    ['#fa985d', '#ffffff']
    }));

    var child = document.getElementById('circles-9');
    circles.push(Circles.create({
        id:         child.id,
        value:      7.7,
        radius:     19,
        width:      3,
        maxValue:   10,
        duration:   1000,
        text:       function(value){return value;},
        colors:    ['#fa985d', '#ffffff']
    }));

    var child = document.getElementById('circles-10');
    circles.push(Circles.create({
        id:         child.id,
        value:      9.2,
        radius:     19,
        width:      3,
        maxValue:   10,
        duration:   1000,
        text:       function(value){return value;},
        colors:    ['#fa985d', '#ffffff']
    }));

    var child = document.getElementById('circles-11');
    circles.push(Circles.create({
        id:         child.id,
        value:      7.4,
        radius:     14,
        width:      3,
        maxValue:   10,
        duration:   1000,
        text:       function(value){return value;},
        colors:    ['#fa985d', '#ffffff']
    }));

    var child = document.getElementById('circles-12');
    circles.push(Circles.create({
        id:         child.id,
        value:      6.5,
        radius:     14,
        width:      3,
        maxValue:   10,
        duration:   1000,
        text:       function(value){return value;},
        colors:    ['#fa985d', '#ffffff']
    }));

    var child = document.getElementById('circles-13');
    circles.push(Circles.create({
        id:         child.id,
        value:      8.6,
        radius:     14,
        width:      3,
        maxValue:   10,
        duration:   1000,
        text:       function(value){return value;},
        colors:    ['#fa985d', '#ffffff']
    }));

    var child = document.getElementById('circles-14');
    circles.push(Circles.create({
        id:         child.id,
        value:      9.3,
        radius:     14,
        width:      3,
        maxValue:   10,
        duration:   1000,
        text:       function(value){return value;},
        colors:    ['#fa985d', '#ffffff']
    }));
  </script>

  <script>
    $('#layerslider').layerSlider({
      autoStart: false,
      responsive: true,
      skinsPath : '',
      imgPreload : false,
      navPrevNext: true,
      navButtons: false,
      hoverPrevNext: false,
      responsiveUnder : 940
    });
  </script>
@endsection    