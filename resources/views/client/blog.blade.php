@extends('layouts.header_footer')

@section('content')

    <!--==============================Breadcrumb================================-->
    <div class="breadcrumb">
      <div class="container">
        <div>
          <span><a href="/  ">Ana Səhifə</a></span> / Məqalələr
        </div>
      </div>
    </div>
    <!--==============================content================================-->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-sm-12">
            <div class="section read_post_list var2">
              <h2 class="section_title section_title_big">Məqalələr</h2>
              <ul>
                <!--Post-->
                @foreach($posts as $post)
                <li>
                  <div class="section_post_left">
                    <div class="scale_image_container">
                      <a href="#"><img src="../images/{{$post->image}}" alt="" class="scale_image"></a>
                    </div>
                    <div class="clearfix">
                      <div class="f_left">
                        <div class="event_date">{{ date('M j, Y', strtotime($post->created_at)) }}</div>
                      </div>
                      <div class="f_right event_info">
                        {{-- @php 
                        $tags=$post->tags(); 
                        print_r($tags);
                        @endphp --}}
                        {{-- @foreach($tags as $tag)
                        <a href="#">
                          <i class="fa fa-comments-o d_inline_m m_right_3"></i> 
                          <span>{{$tag->name}}</span>
                        </a>
                        @endforeach --}}
                      </div>
                    </div>
                    <div class="post_text">
                      <h2 class="post_title"><a href="{{ route('blog.single', $post->slug) }}">{{ $post->title }}</a></h2>
                      <p>{!!substr($post->body,0,250)!!} </p>
                      <a href="{{ route('blog.single', $post->slug) }}" class="button button_type_2 button_grey_light">Ətraflı</a>
                    </div>
                  </div>
                </li>
                @endforeach

              </ul>
            </div>          
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12">
            <!--Featured video-->
            <div class="section">
              <h3 class="section_title">Featured Video</h3>
              <div class="t_align_c">
                <div class="iframe_video_container">
                  <iframe src="http://player.vimeo.com/video/64473966?title=0&amp;byline=0&amp;portrait=0&amp;color=dedede" allowfullscreen></iframe>
                </div>
              </div>
            </div>
            <div class="section t_align_c">
              <a href="#" class="m_top_50 d_block"><img src="images/336x280.jpg" alt=""></a>
            </div>
            <div class="section">
              <h3 class="section_title">Social Media</h3>
              <ul class="social_media_list clearfix">
                <li>
                  <a href="#" class="rss">
                    <i class="fa fa-rss"></i>
                    <div>2,035</div>
                    <p>Subscribers</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="fb">
                    <i class="fa fa-facebook"></i>
                    <div>3,794</div>
                    <p>Fans</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="g_plus">
                    <i class="fa fa-google-plus"></i>
                    <div>941</div>
                    <p>Followers</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="you_tube">
                    <i class="fa fa-youtube-play"></i>
                    <div>7,820</div>
                    <p>Subscribers</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="twitter">
                    <i class="fa fa-twitter"></i>
                    <div>1,562</div>
                    <p>Followers</p>
                  </a>
                </li>
                <li>
                  <a href="#" class="pint">
                    <i class="fa fa-pinterest"></i>
                    <div>1,310</div>
                    <p>Followers</p>
                  </a>
                </li>
              </ul>
            </div>
            <div class="section">
              <h3 class="section_title">Meat Our Writers</h3>
              <ul class="writers_list clearfix">
                <li>
                  <a href="#">
                    <div>
                      <img src="images/writer_1.jpg" alt="">
                    </div>
                    <div class="post_text">
                      <h4>Jessica Priston</h4>
                      <div class="event_date">Writer</div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div>
                      <img src="images/writer_2.jpg" alt="">
                    </div>
                    <div class="post_text">
                      <h4>John Franklin</h4>
                      <div class="event_date">Contributing Editor</div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div>
                      <img src="images/writer_3.jpg" alt="">
                    </div>
                    <div class="post_text">
                      <h4>Camala Haddon</h4>
                      <div class="event_date">Writer</div>
                    </div>
                  </a>
                </li>
              </ul>
            </div>
           
            <div class="section t_align_c">
              <div class="box_image_conteiner">
                <a href="#"><img src="images/125x125.jpg" alt=""></a>
                <a href="#"><img src="images/125x125.jpg" alt=""></a>
                <a href="#"><img src="images/125x125.jpg" alt=""></a>
                <a href="#"><img src="images/125x125.jpg" alt=""></a>
              </div>
            </div>
            <div class="row">
              <!--Weather widget-->
              <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6">
                <div class="section">
                  <h3 class="section_title">Weather Forecast</h3>
                  <div class="weather_widget type2">
                    <div class="weather_info clearfix">
                      <div>
                        <h2>Maribor</h2>
                        <div class="day_number">23<span class="degree">°C</span></div>
                      </div>
                      <div>
                        <p>
                          sunny
                          <br>
                          humidity: 70%
                          <br>
                          wind: omph ENE
                          <br>H 46, L 44</p>
                      </div>
                    </div>
                    <div class="date_list">
                      <ul class="clearfix">
                        <li>
                          <div>26<span class="degree">°C</span></div>
                          <div>FRI</div>
                        </li>
                        <li>
                          <div>24<span class="degree">°C</span></div>
                          <div>SAT</div>
                        </li>
                        <li>
                          <div>25<span class="degree">°C</span></div>
                          <div>SUN</div>
                        </li>
                        <li>
                          <div>28<span class="degree">°C</span></div>
                          <div>MON</div>
                        </li>
                        <li>
                          <div>27<span class="degree">°C</span></div>
                          <div>TUE</div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <!--Calendar-->
              <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6">
                <div class="section calendar">
                  <h3 class="section_title">Calendar</h3>
                  <table>
                    <thead>
                      <tr>
                        <!--titles for td-->
                        <th colspan="7">
                          September 2014
                        </th>
                      </tr>
                    </thead>
                    <tr>
                      <td>M</td>
                      <td>T</td>
                      <td>W</td>
                      <td>T</td>
                      <td>F</td>
                      <td>S</td>
                      <td>S</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td class="link"><a href="#">2</a></td>
                      <td>3</td>
                      <td>4</td>
                      <td>5</td>
                      <td>6</td>
                      <td>7</td>
                    </tr>
                    <tr>
                      <td>8</td>
                      <td>9</td>
                      <td>10</td>
                      <td class="current">11</td>
                      <td>12</td>
                      <td>13</td>
                      <td>14</td>
                    </tr>
                    <tr>
                      <td class="link"><a href="#">15</a></td>
                      <td>16</td>
                      <td>17</td>
                      <td>18</td>
                      <td>19</td>
                      <td>20</td>
                      <td>21</td>
                    </tr>
                    <tr>
                      <td>22</td>
                      <td>23</td>
                      <td>24</td>
                      <td>25</td>
                      <td class="link"><a href="#">26</a></td>
                      <td>27</td>
                      <td>28</td>
                    </tr>
                    <tr>
                      <td class="link"><a href="#">29</a></td>
                      <td>30</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                  <a href="#" class="button button_type_2 button_grey_light">« AUG</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection