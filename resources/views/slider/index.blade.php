@extends('main')

@section('title', '| All Slides')
	
	{!! Html::style('css/select2.min.css') !!}

	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

	<script>
		tinymce.init({
			selector: 'textarea',
			plugins: 'link code',
			menubar: false
		});
	</script>

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>All Slides</h1>
		</div>

		<div class="col-md-2">
			{{-- <a href="{{ route('posts.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Create New Post</a> --}}
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">
			<form action="{{url('/mezunadmin/slider/add')}}" method="post" enctype="multipart/form-data">
				<h2>Add Slide</h2>
				
				{{csrf_field()}}

				<input type="text" name="heading" class="form-control" placeholder="Heading">
				<br>
				<textarea name="description" max-length="100" class="form-control" cols="30" rows="10"></textarea>
				<br>
				<input type="file" name="image" class="form-control">
				<br>
				<input type="submit" value="Add" class="btn btn-success">
				<br><br><br>

			</form>
		</div>

		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Heading</th>
					<th>Description</th>
					<th>Image</th>
					<th>Created at</th>
					<th>Actions</th>
				</thead>

				<tbody>
					
					@foreach ($slides as $slide)
						
						<tr>
							<th>{{ $slide->id }}</th>
							<td>{{ $slide->heading }}</td>
							<td>{!! substr($slide->description,0,100)!!}</td>
							<td>
								<img src="/images/slider/{{$slide->image}}" style="max-width: 50px; max-height: 50px;">
							</td>
							<td>{{ date('M j, Y', strtotime($slide->created_at)) }}</td>
							<td><a href="{{url('mezunadmin/slider/delete',$slide->id)}}" class="btn btn-danger btn-sm">Delete</a></td>
							
						</tr>

					@endforeach

				</tbody>
			</table>

			<div class="text-center">
				{{-- {!! $posts->links(); !!} --}}
			</div>
		</div>
	</div>

@stop