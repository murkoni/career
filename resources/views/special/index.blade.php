@extends('main')

@section('title', '| Welcome')

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>Welcome!</h1>
		</div>

		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">	
			You are in admin panel! Click dropdown, which located in top right side and choose where do you want to visit.
		</div>
	</div>

@stop