@extends('main')

@section('title', '| All Events')

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>All Events</h1>
		</div>

		<div class="col-md-2">
			{{-- <a href="{{ route('posts.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Create New Post</a> --}}
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">
			<form action="{{url('/mezunadmin/events/add')}}" method="post" enctype="multipart/form-data">
				<h2>Add Event</h2>
				
				{{csrf_field()}}

				<input type="text" name="name" class="form-control" placeholder="Name">
				<br>
				<textarea placeholder="About..." name="about" class="form-control" cols="30" rows="10"></textarea>
				<br>
				<input type="date" class="form-control" name="time">
				<br>
				<input type="file" name="logo" class="form-control">
				<br>
				<input type="submit" value="Add" class="btn btn-success">
				<br><br><br>

			</form>
		</div>

		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Name</th>
					<th>About</th>
					<th>Logo</th>
					<th>Time</th>
					<th>Created at</th>
					<th>Actions</th>
				</thead>

				<tbody>
					
					@foreach ($events as $event)
						
						<tr>
							<th>{{ $event->id }}</th>
							<td>{{ $event->title }}</td>
							<td>{{ substr($event->about,0,100)}}</td>
							<td>
								<img src="/images/events/{{$event->photo}}" style="max-width: 50px; max-height: 50px;">
							</td>
							<td>{{$event->time}}</td>
							<td>{{ date('M j, Y', strtotime($event->created_at)) }}</td>
							<td><a href="{{url('mezunadmin/events/delete',$event->id)}}" class="btn btn-danger btn-sm">Delete</a></td>
							
						</tr>

					@endforeach

				</tbody>
			</table>

			<div class="text-center">
				{{-- {!! $posts->links(); !!} --}}
			</div>
		</div>
	</div>

@stop